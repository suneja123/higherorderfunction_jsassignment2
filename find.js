function find(elements, cb) {
    // Do NOT use .includes, to complete this function.
    // Look through each value in `elements` and pass each element to `cb`.
    // If `cb` returns `true` then return that element.
    // Return `undefined` if no elements pass the truth test.
    let found = undefined;
    for(let value of elements)
    {
        if(cb(value))
        {
            return found = value;

        }
    }
    return found;
}
module.exports = {find};