
    // Flattens a nested array (the nesting can be to any depth).
    // Hint: You can solve this using recursion.
    // Example: flatten([1, [2], [3, [[4]]]]); => [1, 2, 3, 4];
        let toFlat = [];
   
        function flat(arr){         
                
                for(let value of arr)
                {
                    if(Array.isArray(value))
                    {
                        flat(value);
                    }
                    else{
                        toFlat.push(value);
                       
                    }
                }
            return toFlat;
            
        }
       
    
    module.exports = {flat};